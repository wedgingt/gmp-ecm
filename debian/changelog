gmp-ecm (7.0.5+ds-1) unstable; urgency=medium

  * New upstream nano version.
  * Debianization:
    - d/watch, harden;
    - d/copyright:
      - copyright year-tuples, update;
      - Files-Excluded list, refresh;
      - Comment field, erase;
    - d/control:
      - Maintainer, now Debian Math Team;
      - Vcs-*, migration to math-team;
      - Standards-Version, bump to 4.6.1 (no change);
      - Build-Depends list:
        - xsltproc and docbook-xsl, add (to generate manpage);
        - libgmp-dev, remove version;
    - d/patches/*:
      - d/p/upstream-fix-manpage-acute_accent.patch, useless;
    - d/rules, harden;
    - d/clean, introduce;
    - d/upstream/metadata, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 06 Jun 2022 14:24:47 +0000

gmp-ecm (7.0.4+ds-6) unstable; urgency=medium

  * Debianization:
    - d/control:
      - Homepage field, update;
      - debhelper, migrate to version 13 (discar d/compat);
      - Rules-Requires-Root, introduce and set to no;
      - Standards-Version, bump to version 4.6.0 (no change);
    - d/rules:
      - override_dh_missing target, provide;
    - d/copyright:
      - Upstream-Contact field, refresh;
      - Source field, update;
      - copyright year tuples, update;
    - d/patches:
      - d/p/upstream-fix-manpage-acute_accent.patch, introduce;
      - d/p/upstream-national_encoding.patch, introduce;
      - d/p/debianization-examples.patch, provide Forwarded field;
    - d/libecm1.lintian-overrides:
      - exit-in-shared-library tag, rename from shlib-calls-exit;
    - d/tests:
      - d/t/{build-examples,make-longcheck}, now use AUTOPKGTEST_TMP;
    - d/watch, migration to gitlab.inria.fr.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 14 Oct 2021 13:33:37 +0000

gmp-ecm (7.0.4+ds-5) unstable; urgency=medium

  * Serious Bug fix release (Closes: #905191), correct typo in extension;
    thanks to Andreas Beckmann <anbe@debian.org> for spotting it.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 28 Oct 2018 00:31:09 +0000

gmp-ecm (7.0.4+ds-4) unstable; urgency=medium

  * Serious Bug fix release (Closes: #905191), correct typo in
    maintscript; thanks to Tobias Hansen <thansen@debian.org> for
    spotting it.
  * Debianization:
    - debian/control:
      - Standards Version, bump to 4.2.1 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 22 Oct 2018 12:54:08 +0000

gmp-ecm (7.0.4+ds-3) unstable; urgency=medium

  * Serious Bug fix release (Closes: #905191), add missing
    'Pre-Depends: ${misc:Pre-Depends}'.
  * Debianization:
    - debian/control:
      - Pre-Depends fields, add (see zeroth point);
      - Vcs-* fields, migration to Salsa;
      - Standards Version, bump to 4.2.0 (no change);
    - debian/rules:
      - get-orig-source target, discrard.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 03 Aug 2018 06:30:09 +0000

gmp-ecm (7.0.4+ds-2) unstable; urgency=medium

  * Debianization:
    - debian/copyright:
      - copyright year tuples, update;
      - Format field, secure;
    - debian/control:
      - debhelper, bump to 11 (basic update);
      - Standards Version, bump to 4.1.3 (no change);
      - Homepage field, secure;
      - cleanup;
    - debian/rules:
      - debhelper, bump to 11:
        - link-doc policy, extra docs now within libecm1-dev ;
        - basic update;
      - gcc-6 build, neutralize (Closes: #892396);
      - get-orig-source target, compression option migrate to d/watch;
    - debian/watch:
      - compression option, migrate from d/rules.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 09 Mar 2018 10:44:30 +0000

gmp-ecm (7.0.4+ds-1) unstable; urgency=medium

  * New upstream patch release.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 14 Oct 2016 00:25:48 +0000

gmp-ecm (7.0.3+ds-1) unstable; urgency=medium

  * New upstream patch release.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 04 Jul 2016 09:38:42 +0000

gmp-ecm (7.0.2+ds-1) unstable; urgency=medium

  * Advanced Tests Failures [i386,armhf] fix release (Closes: #828717).
  * Debianization:
    - debian/copyright:
      - Files-Excluded field, refresh;
    - debian/patches/:
      - d/p/upstream-autotoolization-shlibs.patch, discard as it has been
        integrated, thanks to the upstream maintainer;
    - debian/adhoc/missing/, discard as the material is now provided within
      the upstream source ball, thanks to the upstream maintainer;
    - debian/tests/:
      - d/t/make-longcheck, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 28 Jun 2016 15:41:25 +0000

gmp-ecm (7.0.1+ds-2) unstable; urgency=medium

  * FTBFS[s390x] fix release (Closes: #827034), ad hoc workaround a gcc-5 bug
    not fully isolated.
  * Debianization:
    - debian/rules, force -O3 option.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 15 Jun 2016 14:00:32 +0000

gmp-ecm (7.0.1+ds-1) unstable; urgency=medium

  * New upstream release (Closes: #819702).
  * Debianization:
    - debian/control:
      - Standards-Version, dump to 3.9.8;
      - rationalize (Closes: #819696, #819802);
    - debian/patches/:
      - d/p/upstream-lintian-spelling-error-silence.patch, discard as it has
         been integrated, thanks to the upstream maintainer;
      - d/p/upstream-autotoolization-noexecstack.patch, idem;
      - d/p/upstream-longcheck-neutralize-forgotten-code.patch, discard as it
         has been partially integrated while the non-integrated part was clumsy,
         thanks to the upstream maintainer;
       -d/p/upstream-autotoolization-shlibs.patch, introduce;
    - debian/tests/:
      - d/t/make-longcheck, improve;
    - debian/adhoc/missing/, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 01 Jun 2016 22:28:52 +0000

gmp-ecm (7.0+ds-1) unstable; urgency=medium

  * New major upstream release.
  * Debianization:
    - debian/control:
      - vcs-* fields, secure;
      - Standards-Version, dump to 3.9.7;
      - debug symbol package, drop off in favour of automatic generation;
      - SO name, increment;
    - debian/copyright, update;
    - debian/rules:
      - dpkg-buildflags, add hardening=+all;
      - debug symbol package, drop off (see above);
      - refresh;
    - debian/watch, refresh;
    - debian/adhoc/examples/Makefile, refresh;
    - debian/tests:
      - d/t/make-longcheck, harden and update.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 20 Mar 2016 16:02:29 +0000

gmp-ecm (6.4.4+ds-5) unstable; urgency=medium

  * FTBFS bug fix release (Closes: #806619).
  * Debianization:
    - arch/indep build scheme, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 30 Nov 2015 21:31:59 +0000

gmp-ecm (6.4.4+ds-4) unstable; urgency=medium

  * bug fix release (Closes: #804408):
    - neutralize valgrind check for autopkgtest d/t/make-longcheck .
  * Debianization:
    - debian/control, Vcs-Browser field correction;
    - debian/libecm0.lintian-overrides, typo correction.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 08 Nov 2015 19:13:55 +0000

gmp-ecm (6.4.4+ds-3) unstable; urgency=medium

  * RC bug fix release:
    - RC bug fix (Closes: #793012), faulty assembly code on mips[el] arch
      --- thanks to Jurica Stanojkovic <Jurica.Stanojkovic@imgtec.com>
      for providing the patch;
    - RC bug fix (Closes: #792850), linking misbehaviour when upgrading from
      testing to unstable -- thanks to Andreas Beckmann <anbe@debian.org> for
      noticing the issue.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 21 Oct 2015 04:05:14 +0000

gmp-ecm (6.4.4+ds-2) unstable; urgency=medium

  * RC bug fix release (Closes: #792549), stabilize libdev transition.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 16 Jul 2015 16:07:37 +0000

gmp-ecm (6.4.4+ds-1) unstable; urgency=medium

  * New maintainer (Closes: #771880).
  * Debianization:
    - debian/copyright:
      - in DEP-5 format, bump;
      - Files-Excluded field, introduce;
      - refresh;
    - debian/control:
      - debhelper build-dep to >= 9, bump;
      - Standards Version 3.9.6, bump;
      - Build-Depends field, refresh;
      - Multi-Arch support, achieve;
      - Vcs-* headers, refresh;
    - debian/watch, refresh;
    - debian/rules:
      - full dh integration, refresh;
      - get-orig-source uscan based target which downloads the currently
        packaged upstream tarball and repacks it wrt Files-Excluded;
      - default target which basically queries package status with uscan
        -- output in DEHS format;
    - debian/patches/:
      - patches in DEP-3 format;
      - autotoolization:
        - configure.ac, refresh;
        - master Makefile.am, mark the shared library as not requiring
          executable stack;
      - examples, aka ecmfactor.c, system wide #include;
    - debian/tests/:
      - in DEP-8 format, initiate;
      - simple build test based on examples, introduce;
      - heavy test based on the upstream longcheck ($ make long check),
        introduce;
    - debian/adhoc/examples/:
      - illustrative Makefile, provide;
    - gpg-signature check support, neutralize.
  * This upstream release closes LP: #1294929.

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 30 Jun 2015 11:49:52 +0000

gmp-ecm (6.4.4-2) unstable; urgency=low

  * Apply patch by Logan Rosen to fix FTBFS on ppc64el (closes: #735986).

 -- Laurent Fousse <lfousse@debian.org>  Sun, 09 Feb 2014 13:18:23 -0800

gmp-ecm (6.4.4-1) unstable; urgency=low

  * New upstream release.
    + Fixes build with latest GMP (closes: #713399).

 -- Laurent Fousse <lfousse@debian.org>  Sat, 13 Jul 2013 22:33:00 -0700

gmp-ecm (6.4.2-2) unstable; urgency=low

  * Only break and replace the Error Code Modeler package from Debian
    6.0 (squeeze) that contains its own ecm command, instead of
    conflicting with all versions (patch by Jonathan Nieder)
    Closes: #690276.

 -- Laurent Fousse <lfousse@debian.org>  Mon, 31 Dec 2012 16:28:39 -0800

gmp-ecm (6.4.2-1) unstable; urgency=low

  * New upstream release:
    + Licenses are now GPL3+ or LGPL3+ depending on the files.

 -- Laurent Fousse <lfousse@debian.org>  Sat, 24 Mar 2012 21:03:04 -0700

gmp-ecm (6.4.1~rc3-1) experimental; urgency=low

  * New upstream release candidate.

 -- Laurent Fousse <lfousse@debian.org>  Wed, 14 Mar 2012 19:06:43 -0700

gmp-ecm (6.4.1~rc2e-1) experimental; urgency=low

  * New upstream release candidate.

 -- Laurent Fousse <lfousse@debian.org>  Tue, 13 Mar 2012 20:18:21 -0700

gmp-ecm (6.4.1~rc2-1) experimental; urgency=low

  * New upstream release candidate.

 -- Laurent Fousse <lfousse@debian.org>  Sun, 11 Mar 2012 11:24:28 -0700

gmp-ecm (6.4-1) unstable; urgency=low

  * New upstream release.
  * Drop most patches, already applied upstream.

 -- Laurent Fousse <lfousse@debian.org>  Wed, 29 Feb 2012 19:51:51 -0800

gmp-ecm (6.3-11) unstable; urgency=low

  * Fix SSE2-related FTBFS, thanks Michael Terry for the patch
    (closes: #650545).

 -- Laurent Fousse <lfousse@debian.org>  Sat, 03 Dec 2011 14:12:50 +0100

gmp-ecm (6.3-10) unstable; urgency=low

  * Use the same compiler on all archs.

 -- Laurent Fousse <lfousse@debian.org>  Wed, 28 Sep 2011 08:35:34 +0200

gmp-ecm (6.3-9) unstable; urgency=low

  * Don't build-depend on non-existent packages.

 -- Laurent Fousse <lfousse@debian.org>  Tue, 27 Sep 2011 20:22:38 +0200

gmp-ecm (6.3-8) unstable; urgency=low

  * Properly convert to quilt format (closes: #643134).
  * Upgrade dependency on debhelper.

 -- Laurent Fousse <lfousse@debian.org>  Tue, 27 Sep 2011 00:01:06 +0200

gmp-ecm (6.3-7) unstable; urgency=low

  * Fix dependencies on libgmp-dev (closes: #617207).

 -- Laurent Fousse <lfousse@debian.org>  Tue, 08 Mar 2011 16:16:45 +0100

gmp-ecm (6.3-6) unstable; urgency=low

  * Update build-depends to libgmp-dev (closes: #617207).
  * Modify my email address.

 -- Laurent Fousse <lfousse@debian.org>  Mon, 07 Mar 2011 15:11:45 +0100

gmp-ecm (6.3-5) unstable; urgency=low

  * Disable SSE2 on i386, patch by Matthias Klose (closes: #607686).

 -- Laurent Fousse <laurent@komite.net>  Tue, 21 Dec 2010 11:27:40 +0100

gmp-ecm (6.3-4) unstable; urgency=low

  * Use gcc-4.3 on sparc as 4.4 causes build failure (closes: #593193).
  * Explicitly disable assert (default=yes was upstream's mistake).

 -- Laurent Fousse <laurent@komite.net>  Wed, 01 Sep 2010 11:21:25 +0200

gmp-ecm (6.3-3) unstable; urgency=low

  * Add build depends on m4 for {kfreebsd-,}amd64 (closes: #593021).

 -- Laurent Fousse <laurent@komite.net>  Mon, 16 Aug 2010 11:02:54 +0200

gmp-ecm (6.3-2) unstable; urgency=low

  * Disable assembly on mips.

 -- Laurent Fousse <laurent@komite.net>  Sat, 14 Aug 2010 10:13:15 +0200

gmp-ecm (6.3-1) unstable; urgency=low

  * New upstream version.
  * Switch to dpkg-source 3.0 (quilt) format.
  * DH_COMPAT upgraded to 7.
  * Link libecm0 against libgmp.
  * Upgraded Standards-Version to 3.9.1:
    + support parallel build
  * Conflict with ecm (closes: #580398).

 -- Laurent Fousse <laurent@komite.net>  Fri, 13 Aug 2010 17:15:20 +0200

gmp-ecm (6.2-1) unstable; urgency=low

  * New Upstream Version
  * Fix build failure caused by longlong.h on some architecture,
    patch from upstream r1298 (closes: #483161).

 -- Laurent Fousse <laurent@komite.net>  Wed, 28 May 2008 10:39:04 +0200

gmp-ecm (6.2~rc2-1) unstable; urgency=low

  * New Upstream release candidate.
  * New package libecm0 for dynamic lib (closes: #474083).

 -- Laurent Fousse <laurent@komite.net>  Thu, 15 May 2008 13:17:14 +0200

gmp-ecm (6.1.3-1) unstable; urgency=low

  * New upstream version.
  * Ship the static library in the new libecm-dev package (closes: #464959).
  * Upgraded Standards-Version to 3.7.3, no change needed.
  * Add Homepage field in control.

 -- Laurent Fousse <laurent@komite.net>  Tue, 12 Feb 2008 10:14:02 +0100

gmp-ecm (6.1.2-1) unstable; urgency=low

  * New Upstream Version (closes: #434026).

 -- Laurent Fousse <laurent@komite.net>  Sat, 21 Jul 2007 11:39:43 +0200

gmp-ecm (6.1.1-4) unstable; urgency=low

  * Disable assembly on hppa as well (closes: #379006).

 -- Laurent Fousse <laurent@komite.net>  Mon, 31 Jul 2006 12:29:51 +0200

gmp-ecm (6.1.1-3) unstable; urgency=low

  * The build failure on s390 is of a more complicated nature,
    disable assembly completely until someone fixes it.

 -- Laurent Fousse <laurent@komite.net>  Thu, 20 Jul 2006 13:58:34 +0200

gmp-ecm (6.1.1-2) unstable; urgency=low

  * Define types before inclusion of longlong.h, fixes build
    failure on s390.

 -- Laurent Fousse <laurent@komite.net>  Thu, 20 Jul 2006 10:31:17 +0200

gmp-ecm (6.1.1-1) unstable; urgency=low

  * New upstream release
  * Change debian/copyright to point to new download page.
  * Upgraded Standards-Version to 3.7.2, no change needed.

 -- Laurent Fousse <laurent@komite.net>  Wed, 19 Jul 2006 21:50:56 +0200

gmp-ecm (6.0.1-2) unstable; urgency=low

  * Updated Standards-Version to 3.6.2 (no change needed).
  * Rebuild needed because of GMP C++ transition (closes: #326196).

 -- Laurent Fousse <laurent@komite.net>  Sun,  4 Sep 2005 10:40:16 +0200

gmp-ecm (6.0.1-1) unstable; urgency=low

  * New upstream release.

 -- Laurent Fousse <laurent@komite.net>  Fri,  1 Apr 2005 15:02:22 +0200

gmp-ecm (6.0-2) unstable; urgency=low

  * Applied upstream patches 04 through 08, fixes misc bugs including
    potential segfault.

 -- Laurent Fousse <laurent@komite.net>  Fri, 11 Mar 2005 10:37:00 +0100

gmp-ecm (6.0-1) unstable; urgency=low

  * New upstream release, repackaged since upstream switched to autotools.
  * Applied upstream patch 02, fixes linking problem.
  * Applied upstream patch 03, fixes segfault.
  * Backported test.ecm from CVS, makes tests a lot faster.
  * Add manpage link gmp-ecm(1).
  * Changed to Standards-Version 3.6.1 (no change needed).

 -- Laurent Fousse <laurent@komite.net>  Mon, 28 Feb 2005 16:02:28 +0100

gmp-ecm (5.0.3-3) unstable; urgency=low

  * Fix typo in manpage, thanks Bill Allombert (closes: #254255).

 -- Laurent Fousse <laurent@komite.net>  Thu, 24 Jun 2004 15:18:15 +0200

gmp-ecm (5.0.3-2) unstable; urgency=low

  * Fixed debian/rules, "test" target no longer called twice.
  * Updated to newer Cunningham numbers input list.

 -- Laurent Fousse <laurent@komite.net>  Mon, 26 Jan 2004 13:33:03 +0100

gmp-ecm (5.0.3-1) unstable; urgency=low

  * New upstream release.
  * "test" target added in debian/rules and called at build time.
  * License for two files is LGPL, not GPL.
  * Watch file removed as it is not applicable anymore.

 -- Laurent Fousse <laurent@komite.net>  Sun,  9 Nov 2003 13:10:49 +0100

gmp-ecm (5.0.1-3) unstable; urgency=low

  * Added upstream patch that checks mallocs (closes: #197444).
  * Updated Standards-Version to 3.6.0.

 -- Laurent Fousse <laurent@komite.net>  Sun, 14 Sep 2003 17:26:43 +0200

gmp-ecm (5.0.1-2) unstable; urgency=low

  * Updated manpage. Closes: #197443

 -- Laurent Fousse <laurent@komite.net>  Mon, 16 Jun 2003 08:07:29 +0000

gmp-ecm (5.0.1-1) unstable; urgency=low

  * Initial Release. Closes: #180191

 -- Laurent Fousse <laurent@komite.net>  Sun, 25 May 2003 09:54:44 +0000
